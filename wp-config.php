<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'RpbvGca11dq18Asg+dJYwMgXCm9Qnwz9Le7A/dSWyZbI5UoqmneQyZ5pGZ6lxIlLrEtEFH75pAsm0C73W8vOOw==');
define('SECURE_AUTH_KEY',  'EQbk/XlM6v0u+jjf/kE8ifnWCLKLPa0Lrxyez0HDylMPffS4da3Zud91Jh1xV4TSY4m8Y92An9GXPWGq7wJgrQ==');
define('LOGGED_IN_KEY',    'n0J33EGgxNFprTmqdaCkjfsCqzCjk6vo1PNEi1pD6uReAEVBqAfe8TXYjZRaFeySvdXgEi6lu58vb50kf5eu4g==');
define('NONCE_KEY',        'DYAX7ey/MbVD3CMnO3lxBWYU3PAKC7kMK/qRGzd0tPYM4RIvmJserttiHrSjUw1LRjheQbHhpRUSA8rViDVitg==');
define('AUTH_SALT',        'flCwn4rolcA9+O2r8ZQupQcgwGP/qHG49oYugNsWxx83JJeFYG+H6DDoba88byGGCnTnXr/fbDKVmSg7IZe14w==');
define('SECURE_AUTH_SALT', 'MK5hj6mOMOby17mRICktw2HsYw29ubCRKo+u1qDWnoxfIGYUm/Cs/XGx2OKpIKaf8dmVRlsMLUasH6zlqBfhXw==');
define('LOGGED_IN_SALT',   'VlICFbSxUt1cJfyDuL9EX6fkVSAts94ZMhRCbUmtPDxOsEP4nQBFhgBuEvcJMKJBaniWg4J25ya7yIosVKqXcg==');
define('NONCE_SALT',       'zl1lsLMjmD5ty7t/Xj8wD9N5LYaCuY0S5BFzideutUqS/207afoDUiTOLP5Cg/+TFVHWZlA3NXO4laTFaATAdw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
