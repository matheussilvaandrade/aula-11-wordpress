<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php
            if(is_front_page()){ //home
                echo wp_title(' ');

            } elseif (is_page()) {
                echo wp_title(' '); echo ' - ';

            } elseif (is_search()) {
                echo 'Busca - ';

            } elseif (!(is_404()) && (is_single() || is_page())) {
                wp_title(' '); echo ' - ';

            } elseif (is_404()) {
                echo 'Não encontrada -';

            } bloginfo('name');
        ?>
    </title>
    <link rel="icon" href="<?php wp_site_icon()?>">
    <?php wp_head( ) ?>
</head>
<body>
    <header>
        <nav class="container">
            <a href="#"><img src="<?php echo get_template_directory_uri()?>/assets/img/logoIN.svg" alt="logo"></a>
            <input type="checkbox" id="checkconfig" style="display: none"/>
            <label for="checkconfig" class="nav-icon">
                <div></div>
            </label>
            <?php
                $args = array(
                    'menu' => 'Menu de Navegação',
                    'theme_location' => 'navegacao',
                    'container' => true
                );
                wp_nav_menu($args)
            ?>
        </nav>
    </header>