<?php 
    get_header()
?>   
    <main id="pag404">
        <h1>404</h1>
        <p>Página não encontrada!</p>
        <a href="javascript:history.back()">Voltar para página anterior</a>
    </main>
<?php 
    get_footer()
?>