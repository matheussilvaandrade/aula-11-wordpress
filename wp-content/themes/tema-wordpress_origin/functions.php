<?php
    remove_action ( 'wp_head' , 'rsd_link' ); // remover link de descoberta realmente simples
    remove_action ( 'wp_head' , 'wp_generator' ); // remove a versão do wordpress
    
    remove_action ( 'wp_head' , 'feed_links' , 2 ); // remover links de feed rss (certifique-se de adicioná-los você mesmo se estiver usando feedblitz ou um serviço rss)
    remove_action ( 'wp_head' , 'feed_links_extra' , 3 ); // remove todos os links de feed rss extras
    
    remove_action ( 'wp_head' , 'index_rel_link' ); // remove o link para a página de índice
    remove_action ( 'wp_head' , 'wlwmanifest_link' ); // remove wlwmanifest.xml (necessário para oferecer suporte ao gravador do Windows Live)
    
    remove_action ( 'wp_head' , 'start_post_rel_link' , 10 , 0 ); // remover link de postagem aleatória
    remove_action ( 'wp_head' , 'parent_post_rel_link' , 10 , 0 ); // remover link da postagem pai
    remove_action ( 'wp_head' , 'adjacent_posts_rel_link' , 10 , 0 ); // remove os links da postagem seguinte e anterior
    remove_action ( 'wp_head' , 'adjacent_posts_rel_link_wp_head' , 10 , 0 );
          
    remove_action ( 'wp_head' , 'print_emoji_detection_script' , 7 );
    remove_action ( 'wp_print_styles' , 'print_emoji_styles' );


    function add_styles_and_scripts(){
        wp_enqueue_style('reset-sheet', get_template_directory_uri() . "/assets/css/reset.css");
        wp_enqueue_style('style-sheet', get_template_directory_uri() . "/style.css");
        wp_enqueue_script('mail-go-script', get_template_directory_uri() . "/assets/js/mailgo.min.js");
    }
    add_action('wp_enqueue_scripts', 'add_styles_and_scripts');

    function create_custom_post_news() {
        // Set the labels, this variable is used in the $args array
        $labels = array(
          'name'               => __( 'Noticias' ),
          'singular_name'      => __( 'Noticia' ),
          'add_new'            => __( 'Adicionar nova notícia' ),
          'add_new_item'       => __( 'Adicionar nova notícia' ),
          'edit_item'          => __( 'Editar notícia' ),
          'new_item'           => __( 'Nova notícia' ),
          'all_items'          => __( 'Todas as notícias' ),
          'view_item'          => __( 'Ver notícia' ),
          'search_items'       => __( 'Procurar notícia' ),
          'featured_image'     => 'Poster',
          'set_featured_image' => 'Add Poster'
        );
        // The arguments for our post type, to be entered as parameter 2 of register_post_type()
        $args = array(
          'labels'            => $labels,
          'description'       => 'Notícias quentinhas como Girafa de carro',
          'public'            => true,
          'menu_position'     => 5,
          'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields' ),
          'has_archive'       => true,
          'show_in_admin_bar' => true,
          'show_in_nav_menus' => true,
          'has_archive'       => true,
          'query_var'         => 'film'
        );
        register_post_type( 'news', $args);
    }
    add_action('init', 'create_custom_post_news');
?>